package main

import (
	"fmt"
	"os"
	"star-edge-cloud/edge/scheduler/bussiness"
	"star-edge-cloud/edge/scheduler/interfaces"
	"star-edge-cloud/edge/transport/http"
	"star-edge-cloud/edge/utils/common"

	"github.com/joho/godotenv"
	"github.com/takama/daemon"
)

func main() {
	dir := common.GetCurrentDirectory()
	godotenv.Load(dir + "/conf/scheduler_conf.env")
	serverAddr := os.Getenv("Scheduler.ServerAddr")
	dbpath := os.Getenv("Scheduler.DBPath")

	task1 := &interfaces.TaskImp{DBPath: dbpath}
	server := &http.RestServer{ServerAddr: serverAddr}
	client := &http.RestClient{}
	task1.SetTransportServer(server)
	task1.SetTransportClient(client)
	task1.Manager = bussiness.NewManager()
	task1.Manager.MetadataDBPath = dbpath
	task1.Manager.InitDB(dbpath)
	task1.Manager.MetadataDBPath = fmt.Sprintf("file:%s?cache=shared", dbpath)
	task1.Manager.Load()

	srv, err := daemon.New("scheduler", "规则引擎")
	if err != nil {
		// extlog.WriteLog(err.Error())
		os.Exit(1)
	}
	task1.Daemon = srv
	_, err = task1.Manage()
	if err != nil {
		// extlog.WriteLog(err.Error())
		os.Exit(1)
	}
}
