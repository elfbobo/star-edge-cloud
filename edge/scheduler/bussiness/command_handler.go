package bussiness

import (
	"encoding/json"
	"star-edge-cloud/edge/models"
	"star-edge-cloud/edge/transport/interfaces"
)

// SchedulerCommnadHandler 接收到数据，调用回调方法
type SchedulerCommnadHandler struct {
	Client  interfaces.IClient
	Manager *SchedulerManager
}

// Handle -
func (it *SchedulerCommnadHandler) Handle(request *models.Command, response *models.Response) error {
	response = &models.Response{}
	switch request.Type {
	case "remove":
		id := string(request.Data)
		if err := it.Manager.RemoveSchedulerTask(id); err == nil {
			response.Status = "success"
		} else {
			response.Status = err.Error()
		}
	case "list":
		if collection, err := it.Manager.QueryAllSchedulerTask(); err == nil {
			response.Status = "success"
			response.Message, _ = json.Marshal(collection)
		} else {
			response.Status = err.Error()
		}
	}

	return nil
}
