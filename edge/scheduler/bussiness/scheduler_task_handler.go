package bussiness

import (
	"star-edge-cloud/edge/models"
	"star-edge-cloud/edge/transport/interfaces"
)

// SchedulerTaskHandler 接收到数据，调用回调方法
type SchedulerTaskHandler struct {
	Client  interfaces.IClient
	Manager *SchedulerManager
}

// Handle -
func (it *SchedulerTaskHandler) Handle(request *models.SchedulerTask, response *models.Response) error {
	return it.Manager.AddSchedulerTask(request)
}
