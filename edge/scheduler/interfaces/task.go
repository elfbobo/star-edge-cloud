package interfaces

import (
	"fmt"
	"os"
	"star-edge-cloud/edge/models"
	"star-edge-cloud/edge/scheduler/bussiness"
	tin "star-edge-cloud/edge/transport/interfaces"
	"time"

	"github.com/nosixtools/timewheel"
	"github.com/takama/daemon"
)

// ChStopTask - 停止信号
var ChStopTask = make(chan int)

// TaskImp -
type TaskImp struct {
	daemon.Daemon
	server     tin.IServer
	client     tin.IClient
	stopSignal chan int
	DBPath     string
	Manager    *bussiness.SchedulerManager
}

// SetTransportServer -
func (it *TaskImp) SetTransportServer(server tin.IServer) {
	it.server = server
	it.server.SetSchedulerTaskHandler(&bussiness.SchedulerTaskHandler{Manager: it.Manager})
}

// SetTransportClient -
func (it *TaskImp) SetTransportClient(client tin.IClient) {
	it.client = client
}

// Execute 带参启动一个设备服务，至少需要配置设备Id信息
func (it *TaskImp) Execute() {
	go it.server.Start()
	go func() {
		//初始化时间轮盘
		//参数：interval 时间间隔
		//参数：slotNum  轮盘大小
		tw := timewheel.New(5*time.Second, 120)
		tw.SlotOver = func() {
			// 10分钟以上,刷新扫描时间
			it.Manager.LowPriorityQueue.LastScanTime = time.Now()
			// 查看是否有10分钟以内将执行的任务
			for item, hasNext := it.Manager.LowPriorityQueue.Pop(); hasNext; {
				task := item.(models.SchedulerTask)
				if it.Manager.GetNextTime(&task).Sub(time.Now()) < 600*time.Second {
					it.Manager.HighPriorityQueue.Push(task)
				}
				it.Manager.LowPriorityQueue.Push(task)
			}
		}
		tw.Start()
		//添加定时任务
		//参数：interval 时间间隔
		//参数：times 执行次数 -1 表示周期任务 >0 执行指定次数
		//参数：key 任务唯一标识符 用户更新任务和删除任务
		//参数：taskData 回调函数参数
		//参数：job 回调函数
		// 10分钟内任务
		for item, hasNext := it.Manager.HighPriorityQueue.Pop(); hasNext; {
			task := item.(models.SchedulerTask)
			tw.AddTask(5*time.Second,
				1,
				task.ID,
				timewheel.TaskData{"scheduler": "调度服务"},
				func(params timewheel.TaskData) {
					if task.IsAvailable {
						it.Manager.Execute(&task)
					}
					if task.ExecutingFrequency != models.Once {
						if it.Manager.GetNextTime(&task).Sub(time.Now()) <= 600*time.Second {
							it.Manager.HighPriorityQueue.Push(task)
						} else {
							it.Manager.LowPriorityQueue.Push(task)
						}
					}
				})
		}
	}()

	ChStopTask <- 1
}

// Exit - 回收资源，停止设备
func (it *TaskImp) Exit() error {
	<-ChStopTask
	return nil
}

// Manage by daemon commands or run the daemon
func (it *TaskImp) Manage() (string, error) {
	usage := "Usage: myservice install | remove | start | stop | status"

	// if received any kind of command, do it
	if len(os.Args) > 1 {
		command := os.Args[1]
		switch command {
		case "install":
			return it.Install()
		case "remove":
			return it.Remove()
		case "start":
			return it.Start()
		case "stop":
			return it.Stop()
		case "status":
			str, err := it.Status()
			if err != nil {
				fmt.Println(err.Error())
			}
			fmt.Println(str)
			return str, err
		default:
			return usage, nil
		}
	}
	it.Execute()
	return "", nil
}
