package models

import "time"

// Frequency -
type Frequency int

// iota 初始化后会自动递增
const (
	Once   Frequency = iota // value --> 0
	Minute                  // value --> 1
	Hour
	Day
	Week
	Month
	Year
)

// SchedulerTask - 调度任务
type SchedulerTask struct {
	ID                 string
	Name               string
	Address            string
	Topic              string
	ExecutingFrequency Frequency     // once, minute, hour, day, week, month, year
	Offset             time.Duration //once, minute, hour, day, week, month + offset,单位纳秒（10~-9秒）
	UserID             string
	UserData           string //用户数据
	IsAvailable        bool
}
