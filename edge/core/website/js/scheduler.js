function save() {
    var vm = new Vue({
        el: '#task',
        data: {
            input: {
                task_name: '',
                task_addr: ''
            },
            output: '',
        },
        methods: {
            saveTask: function () {
                // 发送post请求
                this.$http({
                    method: 'post',
                    url: '/api/scheduler/add',
                    params: this.input,
                    credientials: false,
                    emulateJSON: true
                }).then(function (response) {
                    alert(this.output);
                    this.output = response;
                    alert(this.output);
                }, function () {
                    console.log('请求失败处理');
                });
            }
        }
    });
}

function list() {
    var vm = new Vue({
        el: '#tb_tasks',
        data: {
            tasks: {},
            id:'',
        },
        mounted: function () {
            this.getTaskList();
        },
        // create:function(){},
        methods: {
            removeTask: function (id) {
                this.$http({
                    method: 'post',
                    url: '/api/scheduler/remove'
                }).then(function (response) {
                    console.log(response);
                    this.tasks = response;
                }, function () {
                    console.log('请求失败处理');
                });
            },
            getTaskList: function () {
                // 发送post请求
                this.$http({
                    method: 'post',
                    url: '/api/scheduler/list',
                    params: this.id,
                }).then(function (response) {
                    console.log(response);
                }, function () {
                    console.log('请求失败处理');
                });
            }
        }
    });
}